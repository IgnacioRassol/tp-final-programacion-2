#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "funciones.h"

/*
Se representa un laberinto como una lista de listas de enteros, donde cada pared es un
1, cada espacio vacio un 0, y el objetivo un 2.

Se representan las coordenadas como dos enteros, siendo el primero la distancia vertical al origen, 
y el segundo la distancia horizontal al origen, con el origen de cordenadas siendo la esquina superior
izquierda, y tomando como valores positivos hacia abajo y hacia la derecha, respectivamente.

Se define string como un array unidimensional de char.
*/

/*
Se representa palabras como cadenas de caracteres.

menu: char[], char[] -> int

Recibe dos cadenas de caracteres. Le solicita al usuario por consola que ingrese los nombres de los archivos 
de entrada y de salida, y guarda los nombres en las cadenas que recibe como parametros. Se hace una validacion de 
la entrada y la salida antes de finalizar la funcion, y en caso de que sean invalidas se le solicita al usuario
intentar nuevamente, o, en el caso de la entrada, se le da la opcion de finalizar el programa.
La validacion consiste en revisar que lo ingresado no sea solo un enter, y en el caso de la entrada, que el archivo
de entrada efectivamente exista.
*/
int menu(char entrada[], char salida[]){

	int output = -1;
	char respuesta[10];
	FILE *puntero_archivo;


	do {
        printf("Ingrese el nombre del archivo de entrada:\n");
		fgets(entrada, 100, stdin);
		if (entrada[0] == '\n')
			printf("Tiene que ingresar un archivo de entrada, intente nuevamente.\n");

       else{
			entrada[strlen(entrada)-1] = '\0';
			puntero_archivo = fopen(entrada, "r");

			if(puntero_archivo){
				fclose(puntero_archivo);
                output = 1;
			}else{

				do{
					printf("%s\n%s\n%s\n",
						"El archivo ingresado no existe.",
						"Desea intentar nuevamente? (En caso de no hacerlo finalizara el programa)",
						"Respuestas validas: \'si\' o \'no\'");
					scanf("%9s%*c", respuesta);
				}while(strcmp(respuesta, "si") && strcmp(respuesta, "no"));

				if (!strcmp(respuesta, "no"))
					output = 0;
			}

		}
	} while(entrada[0] == '\n' || output == -1);

	if (!output)
		return 0;


	printf("Ingrese el nombre del archivo de salida:\n");

	do {
		fgets(salida, 100, stdin);
		if (salida[0] == '\n')
			printf("Tiene que ingresar un archivo de salida, intente nuevamente.\n");
	} while(salida[0] == '\n');

	salida[strlen(salida)-1] = '\0';


	return output;

}
/*
Se representa coordenadas como enteros, dimensiones como enteros, y valores booleanos como enteros

adentro: int, int, int, int -> int

Toma 4 enteros. Los primeros dos representan las coordenadas de un punto. El segundo par los limites del laberinto.
Devuelve 1 si el punto se encuentra dentro del limite del laberinto, 0 en caso contrario.
*/
int adentro(int p1, int p2, int lab1, int lab2){
	if (p1 >= 0 && p1 < lab1 &&
		p2 >= 0 && p2 < lab2)
		return 1;
	else
		return 0;
}

/*
Se representa nombres de archivo como string, coordenadas de puntos como array de ints, listas de coordenadas 
como lista de array de ints, cantidades como int, y valores booleanos como int

lectura_archivo: char [], int[][], int* -> int

Recibe una string, un array 2d, y un puntero. Guarda el contenido de un archivo que tiene un formato
especifico, cuyo nombre lo indica la string, en el array.
La primer linea del archivo contiene el tamaño del laberinto. Se verifica que la linea tenga dos enteros, y
que dichos enteros sean mayores a 0 y menores o iguales a 15 (el tamaño del laberinto esta acotado dentro de ese rango).
La segunda linea indica las coordenadas del objetivo, y se verifica que tenga dos enteros, y que se encuentre
adentro del laberinto.
De la tercer linea en adelante se encuentran las coordenadas de los obstaculos. La cantidad maxima 
de obstaculos es 20, se lee hasta alcanzar ese numero o el final del archivo, lo que pase primero.
Se verifica que cada linea tenga 2 enteros, y que los obstaculos no coincidan con el objetivo ni que
esten afuera del laberinto.
Se guarda la cantidad de elementos leidos en la direccion de memoria señalada por el puntero.
Si alguna de estas entradas no cumple con alguna de las condiciones, se levanta la bandera de entrada invalida,
se sale del ciclo en el caso de los obstaculos, y se devuelve 0, indicando que la informacion del archivo
de entrada es invalida y no se debe proseguir con el programa.
Si se cumplen todas las condiciones, se devuelve 1, indicando que la entrada era correcta.
Notese que no se chequean si los obstaculos estan repetidos, esto es una decision de diseño.
*/
int lectura_archivo(char  entrada[], int laberinto[][2], int *cant_elementos){
	int aux1, aux2, lab1, lab2, contador = 0, validacion, entrada_invalida = 0;
	FILE *puntero_archivo = fopen(entrada, "r");
    *cant_elementos = 0;

	//Tamaño del laberinto.

	validacion = fscanf(puntero_archivo, "%d%d", &lab1, &lab2);
	if (validacion == 2){
		if (lab1 > 0 && lab1 <= 15 &&
			lab2 > 0 && lab2 <= 15){

			laberinto[contador][0] = lab1;
			laberinto[contador][1] = lab2;
			contador++;
		}
		else{
			printf("%s\n%s\n",
				"El tamaño ingresado del laberinto es invalido, ambas dimensiones deben estar entre 0 y 15.",
				"Modifique el archivo de entrada e intente nuevamente.");
			entrada_invalida = 1;
		}
	}
	else{
		printf("%s\n%s\n",
			"La parte de la entrada correspondiente al tamaño del laberinto esta formateada incorrectamente.",
			"Modifique el archivo de entrada e intente nuevamente.");
		entrada_invalida = 1;
	}

	if (entrada_invalida){
		fclose(puntero_archivo);
		return 0;
	}


	//Ubicacion del objetivo

	validacion = fscanf(puntero_archivo, "%d%d", &aux1, &aux2);

	if (validacion == 2){
		if (adentro(aux1, aux2, lab1, lab2)){
			laberinto[contador][0] = aux1;
			laberinto[contador][1] = aux2;
			contador++;
		}
		else{
			printf("%s\n%s\n",
				"La ubicacion del objetivo es invalida, deberia encontrarse adentro del laberinto.",
				"Modifique el archivo de entrada e intente nuevamente.");

			entrada_invalida = 1;
		}
	}
	else{
		printf("%s\n%s\n",
			"La parte de la entrada correspondiente a la ubicacion del objetivo esta formateada incorrectamente.",
			"Modifique el archivo de entrada e intente nuevamente.");
		entrada_invalida = 1;
	}

	if (entrada_invalida){
		fclose(puntero_archivo);
		return 0;
	}

	//Obstaculos

	while(!feof(puntero_archivo) && contador < 22 && !entrada_invalida){

		validacion = fscanf(puntero_archivo, "%d%d", &aux1, &aux2);

		if (validacion == 2){

			if (adentro(aux1, aux2, lab1, lab2) && (aux1 != laberinto[1][0] || aux2 != laberinto[1][1])){
				laberinto[contador][0] = aux1;
				laberinto[contador][1] = aux2;
			}
			else{
				printf("%s%d%s\n%s\n",
					"La ubicacion del obstaculo ", contador-1, " es invalida.",
					"Modifique el archivo de entrada e intente nuevamente.");

				entrada_invalida = 1;
			}
		}
		else{
			printf("%s%d%s\n%s\n",
				"La ubicacion del obstaculo ", contador-1,
				" esta formateada incorrectamente.",
				"Modifique el archivo de entrada e intente nuevamente.");

			entrada_invalida = 1;
		}
		contador++;
	}

	if (entrada_invalida){
		fclose(puntero_archivo);
		return 0;
	}

	*cant_elementos = contador;
	return 1;

}

/*
Se representa coordenadas de puntos como array de ints, listas de coordenadas como lista de array de ints,
laberintos como arrays 2d de enteros, y cantidades como int.

llenar_laberinto: int[][], int[][], int

Se reciben los datos para completar el laberinto, el laberinto a completar, y la cantidad de elementos que 
contiene el primer array.
Se guardan en variables separadas las dimensiones del laberinto, y las coordenadas del objetivo.
Luego se completan con 0 todos los espacios del laberinto a completar que se encuentran acotados superiormente
por las dimensiones del laberinto.
Una vez hecho eso, se pone un 2 en la posicion del objetivo, y se itera sobre los obstaculos, poniendo un 1
en la posicion de cada uno.
*/
void llenar_laberinto(int laberinto_entrada[][2], int laberinto_salida[][15], int cant_elementos){
	int largo, alto, objetivox, objetivoy, i, j;

	largo = laberinto_entrada[0][0];
	alto = laberinto_entrada[0][1];
	objetivox = laberinto_entrada[1][0];
	objetivoy = laberinto_entrada[1][1];

	for (i = 0; i < largo; i++)
		for (j = 0; j < alto; j++)
			laberinto_salida [i][j] = 0;

	laberinto_salida[objetivox][objetivoy] = 2;

	for (i = 2; i < cant_elementos; i++){
		laberinto_salida[laberinto_entrada[i][0]][laberinto_entrada[i][1]] = 1;
	}

}

/*
Se representa un laberinto como un array bidimensional de enteros, dimensiones como int, y nombres
de archivo como string.

escritura_archivo: int[][], int, int, char[]
Recibe el laberinto, las dimensiones del laberinto, y el nombre del archivo de salida.
Guarda los datos del laberinto en el archivo de salida.
*/
void escritura_archivo(int laberinto_salida[][15], int largo, int alto, char salida[]){

	FILE *puntero_archivo = fopen(salida, "w");
	int i, j;

	for(i = 0; i < largo; i++){
		for (j = 0; j < alto; j++)
			fprintf(puntero_archivo ,"%d", laberinto_salida[i][j]);

		fprintf(puntero_archivo ,"\n");
	}


	fclose(puntero_archivo);
}
