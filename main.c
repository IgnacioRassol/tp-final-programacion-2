#include <stdio.h>
#include <stdlib.h>

#include "funciones.h"

int main(){
	char entrada[100], salida[100];
	int laberinto_entrada[30][2], laberinto_salida[15][15], cant_elementos, validacion;


	validacion = menu(entrada, salida);
	if (!validacion)
		return 0;

	validacion = lectura_archivo(entrada, laberinto_entrada, &cant_elementos);
	if (!validacion)
		return 0;

	llenar_laberinto(laberinto_entrada, laberinto_salida, cant_elementos);
	escritura_archivo(laberinto_salida, laberinto_entrada[0][0], laberinto_entrada[0][1], salida);

	return 0;
}