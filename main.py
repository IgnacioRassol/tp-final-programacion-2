from resolver import resolver
from os.path import isfile
from pprint import pprint

"""
Se representan rutas de archivo como string
menu: -> string, string
Permite el ingreso por consola de la ruta de los archivos de entrada y de salida, y luego los devuelve.
Valida que se ingrese algo en ambos casos, y el que el archivo exista en el caso de la entrada
"""
def menu():
	entrada = input('Ingrese la ruta del archivo de entrada: ')
	while entrada == '' or not isfile(entrada):
		entrada = input('Ruta invalida. Ingrese la ruta del archivo de entrada: ')

	salida = input('Ingrese la ruta del archivo de salida:  ')
	while salida == '':
		salida = input('Ruta invalida. Ingrese la ruta del archivo de salida: ')

	return entrada, salida

"""
entrada_desde_archivo: string -> int list list
Recibe la ruta de un archivo, y devuelve una lista de listas que contiene todos los numeros de ese archivo
siendo cada una de las listas de numeros una linea del archivo.
No hace ningun tipo de validacion porque asume que recibe como entrada un archivo de salida del generador de
laberintos.
"""
def entrada_desde_archivo(ruta_archivo):
	matriz = []
	with open(ruta_archivo, encoding="utf-8") as archivo:       
		for linea in archivo.read().split():
			fila = [int(caracter) for caracter in linea]
			matriz.append(fila)

	return matriz

"""
salida_a_archivo: (int, int) list list, string
Recibe una lista de posiciones (la solucion del laberinto) y la ruta de un archivo.
Imprime la solucion de forma bonita en el archivo de salida
"""
def salida_a_archivo(solucion, ruta_archivo):
	with open(ruta_archivo, 'w', encoding="utf-8") as archivo:
		pprint(solucion, archivo)

"""
No recibe ni devuelve nada. Llama al menu para obtener las rutas de los archivos del usuario, obtiene el laberinto
desde el archivo de entrada especificado, llama a la funcion que lo resuelve, y de haber solucion la guarda en el archivo de salida, 
de lo contrario informa en pantalla.
"""
def main():
	entrada, salida = menu()

	laberinto = entrada_desde_archivo(entrada)
	solucion = resolver(laberinto)

	if solucion:
		salida_a_archivo(solucion, salida)
	else:
		print("El laberinto ingresado no posee solucion, el archivo de salida no fue alterado.")



if __name__ == "__main__":
	main()