"""
Se representa un laberinto como una lista de listas de enteros, donde cada pared es un
1, cada espacio vacio un 0, y el objetivo un 2.

Se representan los pasos necesarios para resolver el laberinto (llamado 'recorrido' en el codigo)
como una lista de listas de none, de las mismas dimensiones que el laberinto, y la cual se va completando
cada nodo 'visto' (es decir, que se visito algun nodo adyacente y se sabe que se puede seguir por ahi), 
con el nodo que lo 'vio' la primera vez. La unica excepcion a esto es la ubicacion (0, 0), la cual es
la posicion inicial, y se considera que se descubre a si misma.
En la signatura de datos se le hace referencia como '(int, int) list list', pero inicialmente comienza siendo
una lista de listas de None (y no siempre se llena completamente de posiciones).

Se representa el camino a tomar para llegar al objetivo como una lista de tuplas de entero, siendo esta la 
lista de todas las posiciones por las cuales hay que pasar desde la entrada hasta el objetivo.

Se representan las posiciones como una tupla de enteros, de la forma (i, j), siendo i la distancia vertical
al origen, y j la distancia horizontal al origen, con el origen de cordenadas siendo la esquina superior
izquierda, y tomando como valores positivos hacia abajo y hacia la derecha, respectivamente. Esta referido como
'pos' en el codigo.

Se representan las dimensiones del laberinto como enteros (referidos como alto y ancho en el codigo)

Se define adyacente como la posicion que se encuentra inmediatamente arriba, abajo, a la izquierda o a la derecha.
No se cuentan las diagonales.

La forma de resolver el laberinto consiste en, partiendo de una posicion inicial, revisar las posiciones adyacentes
para verificar que se puede 'avanzar' a dicha posicion, agregar esas posiciones a la lista de posiciones a las que se puede avanzar,
y a la matriz de posiciones descubiertas, y luego avanzar hacia alguna de los posiciones adyacentes, y repetir hasta que se encuentre
el objetivo, o se explore todo el laberinto, lo que suceda primero. Se prioriza avanzar a las posiciones que fueron descubiertas mas
recientemente.
"""

"""
inicializar_recorrido: int list list -> None list list
Recibe una lista de listas de enteros, y devuelve una lista de listas de None,
del mismo tamaño que la lista recibida originalmente.
"""
def inicializar_recorrido(laberinto):
	alto, ancho = len(laberinto), len(laberinto[0])

	return [[None for i in range(ancho)] for j in range(alto)]

"""
esta_adentro: (int, int), int, int -> bool
Recibe una tupla de enteros que representa una posicion, y otros dos enteros que representan
los limites de un area(el laberinto), y devuelve verdadero en caso de que la posicion este dentro 
de esa area, o falso de lo contrario
"""
def esta_adentro(pos, alto, ancho):
	i, j = pos
	return 0 <= i < alto and 0 <= j < ancho

"""
validar_posicion: (int, int), int list list, (int, int) list list, int, int -> bool
Recibe una posicion, un laberinto, un recorrido, y las dimensiones de los ultimos dos (las cuales son iguales)
y devuelve verdadero si dicha posicion cumple las siguientes condiciones:
-Estar dentro de los limites del laberinto
-No ser una pared
-No haber sido 'visto' por otro nodo previamente
En caso de no cumplir alguna de las 3 condiciones, devuelve falso
"""
def validar_posicion(pos, laberinto, recorrido, alto, ancho):
	if esta_adentro(pos, alto, ancho):
		no_es_pared = laberinto[pos[0]][pos[1]] != 1
		no_fue_visitado = not recorrido[pos[0]][pos[1]]

		return no_es_pared and no_fue_visitado
	else:
		return False

"""
obtener_posicion_adyacente: (int, int), int list list, (int, int) list list -> (int, int) list
Recibe una posicion, un laberinto, y un recorrido, y devuelve una lista de las posiciones que cumplen las
siguientes condiciones:
-Ser adyacente a la posicion recibida
-Estar dentro de los limites del laberinto
-No ser una pared
-No haber sido 'visto' por otro nodo previamente
Si ninguna posicion cumple estas condiciones, devuelve una lista vacia.
"""
def obtener_posicion_adyacente(pos, laberinto, recorrido):
	i, j = pos
	alto, ancho = len(laberinto), len(laberinto[0])

	posiciones_posibles = [(i, j+1),
						   (i, j-1),
						   (i+1, j),
						   (i-1, j)]

	posiciones_posibles[:] = [x for x in posiciones_posibles if validar_posicion(x, laberinto, recorrido, alto, ancho)]

	return posiciones_posibles

"""
actualizar_recorrido: (int, int) list list, (int, int) list, (int, int)
Recibe un recorrido, una lista de posiciones, y una posicion x.
Guarda en el recorrido que las posiciones de la lista de posiciones fueron
descubiertas por la posicion x.
"""
def actualizar_recorrido(recorrido, pos_adyacentes, posicion_original):
	for posicion in pos_adyacentes:
		i, j = posicion
		recorrido[i][j] = posicion_original


"""
encontrar_camino_final: (int, int) list list, (int, int) -> (int, int) list
Recibe un recorrido y la posicion donde se encuentra el objetivo. Devuelve una lista de todas las 
posiciones por las cuales hay que pasar desde la entrada hasta el objetivo.
Para obtener esta lista, recorre el recorrido desde el final hasta el principio, avanzando al primer nodo 
que descubrio su posicion actual.
"""
def encontrar_camino_final(recorrido, pos):
	camino = []
	i, j = pos

	while (i, j) != (0, 0):
		camino.append((i, j))
		i, j = recorrido[i][j]

	camino.append((0, 0))

	return camino[::-1]

"""
resolver: int list list -> (int, int) list
Recibe un laberinto. Devuelve una lista de todas las posiciones por las cuales hay que 
pasar desde la entrada hasta el objetivo, incluyendo ambos.
"""

def resolver(laberinto):
	#Inicializa todas las variables
	pos = (0, 0) #Posicion en la que se encuentra
	pos_siguientes = [] #Posiciones a las que puede avanzar
	pos_adyacentes = [] #Posiciones adyacentes al nodo en el que se encuentra, a las cuales se puede avanzar
	camino_final = [] #El camino desde la entrada al objetivo
	objetivo_alcanzado = False #Bandera que indica si se encontro el objetivo
	
	#Se inicializa el recorrido
	recorrido = inicializar_recorrido(laberinto) #Esto crea el recorrido del tamaño del laberinto y lo llena de None
	recorrido[0][0] = (0, 0) 
	
	pos_siguientes = obtener_posicion_adyacente(pos, laberinto, recorrido) #Se obtiene las primeras posiciones adyacentes para agregar a pos_siguientes
	actualizar_recorrido(recorrido, pos_siguientes, pos) #marca los nodos descubiertos en la linea anterior como vistos desde el primer nodo

	while not objetivo_alcanzado and pos_siguientes:
		pos = pos_siguientes.pop() #Avanza a la siguiente posicion, y la saca de la lista de posiciones a las que se puede avanzar
		
		objetivo_alcanzado = laberinto[pos[0]][pos[1]] == 2 #Revisa si la posicion actual es el objetivo, y actualiza el valor de la bandera segun corresponda
		pos_adyacentes = obtener_posicion_adyacente(pos, laberinto, recorrido) #busca los nodos validos adyacentes a la nueva posicion
		actualizar_recorrido(recorrido, pos_adyacentes, pos) #actualiza el recorrido con los nuevos nodos descubiertos
		pos_siguientes.extend(pos_adyacentes) #los agrega a la lista de posiciones por visitar

#Repite los pasos del while hasta alcanzar el objetivo o revisar todas las posiciones accesibles del laberinto
#Si alcanzo el objetivo, busca el camino final, de lo contrario el camino queda vacio.

	if objetivo_alcanzado:
		camino_final = encontrar_camino_final(recorrido, pos)

#Devuelve el camino final

	return camino_final