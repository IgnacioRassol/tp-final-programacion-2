from resolver import *

def test_inicializar_recorrido():
	tamaños = [(10, 5), (5, 6), (14, 5), (8, 13), (11, 7), (2, 1), (8, 9)]
	
	for elemento in tamaños:
		alto, ancho = elemento[0], elemento[1]
		laberinto = [[0] * ancho] * alto

		recorrido = inicializar_recorrido(laberinto)

		assert len(recorrido) == alto
		for fila in recorrido:
			assert len(fila) == ancho

			for espacio in fila:
				assert espacio == None

def test_esta_adentro():
	assert esta_adentro((10, 4), 10, 5) == False
	assert esta_adentro((10, 10), 5, 6) == False
	assert esta_adentro((12, 5), 14, 5) == False
	assert esta_adentro((8, 12), 8, 13) == False
	assert esta_adentro((2, 2), 11, 7)  == True
	assert esta_adentro((1, 1), 2, 1)   == False
	assert esta_adentro((1, 5), 8, 9)   == True
	assert esta_adentro((4, 10), 6, 12) == True
	assert esta_adentro((3, 6), 4, 7)   == True

def test_validar_posicion():
	laberinto = [[0, 0, 0, 0, 0],
				 [1, 0, 1, 1, 1],
				 [0, 0, 0, 0, 2]]

	alto, ancho = len(laberinto), len(laberinto[0])

	recorrido = [[(0, 0), (0, 0), (0, 1), None, None],
				 [None, (0, 1), None, None, None],
				 [None, (1, 1), None, None, None]]

	assert validar_posicion((0, 3), laberinto, recorrido, alto, ancho)  == True
	assert validar_posicion((2, 0), laberinto, recorrido, alto, ancho)  == True
	assert validar_posicion((2, 2), laberinto, recorrido, alto, ancho)  == True
	assert validar_posicion((-1, 0), laberinto, recorrido, alto, ancho) == False
	assert validar_posicion((0, -1), laberinto, recorrido, alto, ancho) == False
	assert validar_posicion((1, 1), laberinto, recorrido, alto, ancho)  == False
	assert validar_posicion((1, 0), laberinto, recorrido, alto, ancho)  == False
	assert validar_posicion((0, 5), laberinto, recorrido, alto, ancho)  == False
	assert validar_posicion((3, 1), laberinto, recorrido, alto, ancho)  == False

def test_obtener_posicion_adyacente():
	laberinto = [[0, 0, 0, 0, 0],
				 [1, 0, 1, 1, 1],
				 [0, 0, 0, 0, 2]]
	
	recorrido = [[(0, 0), (0, 0), (0, 1), None, None],
				 [None, (0, 1), None, None, None],
				 [None, (1, 1), None, None, None]]


	assert obtener_posicion_adyacente((2, 1), laberinto, recorrido) == [(2, 2), (2, 0)]
	assert obtener_posicion_adyacente((2, 2), laberinto, recorrido) == [(2, 3)]
	assert obtener_posicion_adyacente((0, 0), laberinto, recorrido) == []
	assert obtener_posicion_adyacente((0, 1), laberinto, recorrido) == []
	assert obtener_posicion_adyacente((0, 2), laberinto, recorrido) == [(0, 3)]

	recorrido = [[(0, 0), None, None, None, None],
				 [None, None, None, None, None],
				 [None, None, None, None, None]]

	assert obtener_posicion_adyacente((0, 1), laberinto, recorrido) == [(0, 2), (1, 1)]
	assert obtener_posicion_adyacente((2, 1), laberinto, recorrido) == [(2, 2), (2, 0), (1, 1)]
	assert obtener_posicion_adyacente((1, 1), laberinto, recorrido) == [(2, 1), (0, 1)]


def test_actualizar_recorrido():
	recorrido = [[(0, 0), None, None, None, None],
				 [None, None, None, None, None],
				 [None, None, None, None, None]]


	recorrido_cp =  [fila[:] for fila in recorrido]
	actualizar_recorrido(recorrido_cp, [(0, 4), (2, 1), (2, 4)], (0, 1)) 

	assert recorrido_cp == [[(0, 0), None, None, None, (0, 1)],
					 [None, None, None, None, None],
					 [None, (0, 1), None, None, (0, 1)]]

	recorrido_cp = [fila[:] for fila in recorrido]
	actualizar_recorrido(recorrido_cp, [(0, 1), (1, 0), (1, 2), (2, 1)], (1, 1))

	assert recorrido_cp == [[(0, 0), (1, 1), None, None, None],
							[(1, 1), None, (1, 1), None, None],
							[None, (1, 1), None, None, None]]

	actualizar_recorrido(recorrido_cp, [(1, 4), (1, 2)], (1, 3))

	assert recorrido_cp == [[(0, 0), (1, 1), None, None, None],
							[(1, 1), None, (1, 3), None, (1, 3)],
							[None, (1, 1), None, None, None]]



def test_encontrar_camino_final():
	recorrido = [[(0, 0), (0, 0), (0, 1), None, None, None],
				 [None, (0, 1), None, None, None, None],
				 [(2, 1), (1, 1), (2, 1), None, None, None],
				 [(2, 0), None, (4, 2), None, (4, 4), None],
				 [(3, 0), (4, 0), (4, 1), (4, 2), (4, 3), (4, 4)]]

	assert encontrar_camino_final(recorrido, (3, 4)) == [(0, 0),
														 (0, 1),
														 (1, 1),
														 (2, 1),
														 (2, 0),
														 (3, 0),
														 (4, 0),
														 (4, 1),
														 (4, 2),
														 (4, 3),
														 (4, 4),
														 (3, 4)]

	

	recorrido = [[(0, 0), (0, 0), (0, 1), None, None, (0, 6), (1, 6)],
				 [None, (0, 1), None, None, (2, 4), None, (2, 6)],
				 [(2, 1), (1, 1), (2, 1), None, (2, 5), (2, 6), (3, 6)],
				 [(2, 0), None, (2, 2), None, None, None, (4, 6)],
				 [(3, 0), None, (3, 2), (4, 2), (4, 3), (4, 4), (4, 5)]]


	assert encontrar_camino_final(recorrido, (1, 4)) == [(0, 0),
														 (0, 1),
														 (1, 1),
														 (2, 1),
														 (2, 2),
														 (3, 2),
														 (4, 2),
														 (4, 3),
														 (4, 4),
														 (4, 5),
														 (4, 6),
														 (3, 6),
														 (2, 6),
														 (2, 5),
														 (2, 4),
														 (1, 4)]

	recorrido = [[(0, 0), (0, 0), (0, 1), None, None, (1, 5), None, (1, 7)],
				 [None, (0, 1), None, None, (2, 4), (1, 4), (1, 5), (1, 6)],
				 [(2, 1), (1, 1), (2, 1), None, (3, 4), (2, 4), None, (1, 7)],
				 [(2, 0), None, (2, 2), None, (4, 4), None, None, (2, 7)],
				 [(3, 0), None, (3, 2), (4, 2), (4, 3), None, (4, 7), (3, 7)]]


	assert encontrar_camino_final(recorrido, (4, 6)) == [(0, 0),
														 (0, 1),
														 (1, 1),
														 (2, 1),
														 (2, 2),
														 (3, 2),
														 (4, 2),
														 (4, 3),
														 (4, 4),
														 (3, 4),
														 (2, 4),
														 (1, 4),
														 (1, 5),
														 (1, 6),
														 (1, 7),
														 (2, 7),
														 (3, 7),
														 (4, 7),
														 (4, 6)]


def test_resolver():
	laberinto = [[0, 0, 0, 0], 
				 [1, 1, 0, 0], 
				 [2, 0, 0, 0]]

	solucion = [(0, 0), (0, 1), (0, 2), (1, 2), (2, 2), (2, 1), (2, 0)]

	assert resolver(laberinto) == solucion

	laberinto = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				 [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
				 [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
				 [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
				 [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
				 [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
				 [0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 0]]

	solucion = [(0, 0),
				(1, 0),
				(2, 0),
				(3, 0),
				(4, 0),
				(5, 0),
				(6, 0),
				(6, 1),
				(6, 2),
				(6, 3),
				(6, 4),
				(6, 5),
				(6, 6),
				(6, 7)]

	assert resolver(laberinto) == solucion

	laberinto = [[0, 0, 0, 1, 1, 0, 1, 0],
				 [1, 0, 1, 1, 0, 0, 0, 0],
				 [0, 0, 0, 1, 0, 0, 1, 0],
				 [0, 1, 0, 1, 0, 1, 1, 0],
				 [0, 1, 0, 0, 0, 1, 2, 0]]

	solucion = [(0, 0),
				(0, 1),
				(1, 1),
				(2, 1),
				(2, 2),
				(3, 2),
				(4, 2),
				(4, 3),
				(4, 4),
				(3, 4),
				(2, 4),
				(1, 4),
				(1, 5),
				(1, 6),
				(1, 7),
				(2, 7),
				(3, 7),
				(4, 7),
				(4, 6)]

	assert resolver(laberinto) == solucion

	laberinto = [[0, 0, 0, 0], 
				 [1, 1, 0, 0], 
				 [2, 1, 0, 0]]

	assert resolver(laberinto) == []